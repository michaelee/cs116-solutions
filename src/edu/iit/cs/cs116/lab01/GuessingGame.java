package edu.iit.cs.cs116.lab01;

import java.util.Scanner;

public class GuessingGame {

	public static void main(String[] args) {
		int secret = new java.util.Random().nextInt(100);
		int guess, prevGuess, diff;
		
		Scanner stdin = new Scanner(System.in);
		
		guess = prevGuess = -1;
		
		do {
			if (guess != -1) {
				if (prevGuess != -1) {
					diff = Math.abs(prevGuess - secret) - Math.abs(guess - secret);
					if (diff < 0) {
						System.out.println("Colder");
					} else if (diff >= 0) {
						System.out.println("Warmer");
					}
				} else {
					System.out.println("Nope");
				}
			}
			System.out.print("Enter a guess: ");
			prevGuess = guess;
			guess = stdin.nextInt();
		} while (guess != secret);

		System.out.println("Congrats!");
		
		stdin.close();
	}

}
