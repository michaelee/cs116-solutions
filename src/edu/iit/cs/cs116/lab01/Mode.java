package edu.iit.cs.cs116.lab01;
import java.util.Scanner;

public class Mode {

	public static void main(String[] args) {
		int[] values = new int[10];
		Scanner stdin = new Scanner(System.in);
		for (int i=0; i<10; i++) {
			System.out.printf("Enter value %d: ", i+1);
			values[i] = stdin.nextInt();
		}
		stdin.close();
		
		// since we don't (officially) know how to sort, we'll take multiple 
		// passes to "guess" the mode
		int mode=0, freq = 0;
		for (int i=0; i<10; i++) {
			int contender = values[i],
				contenderFreq = 0;
			for (int j=0; j<10; j++) {
				if (values[j] == contender)
					contenderFreq++;
			}
			if (contenderFreq > freq) {
				mode = contender;
				freq = contenderFreq;
			}
		}
		System.out.printf("The mode is %d\n", mode);
	}

}
