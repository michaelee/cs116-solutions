package edu.iit.cs.cs116.lab02;
import java.util.Scanner;

public class Room {
	int id;
	String title;
	String description;
	String backDescription;
	String forwardDescription;
	
	public Room(Scanner scanner) {
		id = scanner.nextInt();
		scanner.nextLine();
		title = scanner.nextLine();
		description = scanner.nextLine() + "\n";
		String line = scanner.nextLine();
		while (!line.trim().equals(",")) {
			description += line + "\n";
			line = scanner.nextLine();
		}
		scanner.next(); // throw away the direction
		backDescription = scanner.nextLine().trim();
		scanner.next();
		forwardDescription = scanner.nextLine().trim();
		scanner.nextLine(); // throw away the end of the room ('.')
	}



	public String getTitle() {
		return title;
	}

	public String getDescription() {
		return description;
	}

	public String getBackDescription() {
		return backDescription;
	}

	public String getForwardDescription() {
		return forwardDescription;
	}

	
}
