package edu.iit.cs.cs116.lab02;

import java.util.Scanner;
import java.io.*;

public class REPL {
	static Room r1, r2, r3, currRoom;
	
	public static void advanceToRoom(Room room) {
		currRoom = room;
		System.out.println("You are now in the " + currRoom.getTitle());
	}
	
	public static boolean processCommand(String cmd) {
		switch(cmd) {
		case "l":
			System.out.println("\n" + currRoom.getTitle() + ":\n");
			System.out.println("You are in " 
					+ currRoom.getDescription());
			System.out.println("In front, there is " 
					+ currRoom.getForwardDescription());
			System.out.println("Behind, there is " 
					+ currRoom.getBackDescription());
			System.out.println();
			break;
		case "f":
			if (currRoom == r1) 
				advanceToRoom(r2);
			else if (currRoom == r2) 
				advanceToRoom(r3);
			else
				System.out.println("Can't move forward from here.");
			break;
		case "b":
			if (currRoom == r2) 
				advanceToRoom(r1);
			else if (currRoom == r3)
				advanceToRoom(r2);
			else
				System.out.println("Can't move back from here.");
			break;
		case "q":
			System.out.println("Quitting!");
			return true;
		}
		return false;
	}

	public static void main(String[] args) throws Exception {
		boolean gameOver = false;
		Scanner stdin = new Scanner(System.in);
		Scanner fileScanner = new Scanner(new File("datafiles/rooms-v1.dat"));
		
		r1 = new Room(fileScanner);
		r2 = new Room(fileScanner);
		r3 = new Room(fileScanner);
		fileScanner.close();
		
		currRoom = r1;
		
		while (!gameOver) { // REPL
			System.out.print("Enter a command: ");
			String cmd = stdin.next();
			gameOver = processCommand(cmd);
		}
		stdin.close();
	}
}
