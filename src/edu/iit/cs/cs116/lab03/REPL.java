package edu.iit.cs.cs116.lab03;

import java.util.Scanner;
import java.io.*;

public class REPL {
	static Room currRoom;
	static Room[] rooms;
	
	public static void advanceToRoom(Room room) {
		currRoom = room;
		System.out.println("You are now in the " + currRoom.getTitle());
	}
	
	public static boolean processCommand(String cmd) {
		switch(cmd) {
		case "l":
			System.out.println("\n" + currRoom.getTitle() + ":\n");
			System.out.println("You are in " 
					+ currRoom.getDescription());
			if (currRoom.getExitDestination(Room.NORTH) != -1)
				System.out.println("To the north, there is "
						+ currRoom.getExitDescription(Room.NORTH));
			if (currRoom.getExitDestination(Room.SOUTH) != -1)
				System.out.println("To the south, there is "
						+ currRoom.getExitDescription(Room.SOUTH));
			if (currRoom.getExitDestination(Room.EAST) != -1)
				System.out.println("To the east, there is "
						+ currRoom.getExitDescription(Room.EAST));
			if (currRoom.getExitDestination(Room.WEST) != -1)
				System.out.println("To the west, there is "
						+ currRoom.getExitDescription(Room.WEST));
			System.out.println();
			break;
		case "n":
			if (currRoom.getExitDestination(Room.NORTH) == -1)
				System.out.println("Can't move north from here");
			else 
				advanceToRoom(rooms[currRoom.getExitDestination(Room.NORTH)-1]);
			break;
		case "s":
			if (currRoom.getExitDestination(Room.SOUTH) == -1)
				System.out.println("Can't move south from here");
			else 
				advanceToRoom(rooms[currRoom.getExitDestination(Room.SOUTH)-1]);
			break;
		case "e":
			if (currRoom.getExitDestination(Room.EAST) == -1)
				System.out.println("Can't move east from here");
			else 
				advanceToRoom(rooms[currRoom.getExitDestination(Room.EAST)-1]);
			break;
		case "w":
			if (currRoom.getExitDestination(Room.WEST) == -1)
				System.out.println("Can't move west from here");
			else 
				advanceToRoom(rooms[currRoom.getExitDestination(Room.WEST)-1]);
			break;
		case "q":
			System.out.println("Quitting!");
			return true;
		}
		return false;
	}

	public static void main(String[] args) throws Exception {
		boolean gameOver = false;
		Scanner stdin = new Scanner(System.in);
		Scanner fileScanner = new Scanner(new File("datafiles/rooms-v2.dat"));
		
		rooms = new Room[fileScanner.nextInt()];
		fileScanner.nextLine(); // newline after int
		fileScanner.nextLine(); // throw away '.'
		
		for (int i=0; i<rooms.length; i++) {
			rooms[i] = new Room(fileScanner);
		}
		
		fileScanner.close();
		
		currRoom = rooms[0];
		
		while (!gameOver) { // REPL
			System.out.print("Enter a command: ");
			String cmd = stdin.next();
			gameOver = processCommand(cmd);
		}
		stdin.close();
	}
}
