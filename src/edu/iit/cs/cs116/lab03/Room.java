package edu.iit.cs.cs116.lab03;
import java.util.Arrays;
import java.util.Scanner;

public class Room {
	int id;
	String title;
	String description;
	
	int[] exitDestinations;
	String[] exitDescriptions;
	
	public static final int NORTH = 0;
	public static final int SOUTH = 1;
	public static final int EAST  = 2;
	public static final int WEST  = 3;
	
	public Room(Scanner scanner) {
		exitDestinations = new int[4];
		Arrays.fill(exitDestinations, -1);
		exitDescriptions = new String[4];
		
		id = scanner.nextInt();
		scanner.nextLine();
		title = scanner.nextLine();
		description = scanner.nextLine() + "\n";

		String line = scanner.nextLine();
		while (!line.trim().equals(",")) {
			description += line + "\n";
			line = scanner.nextLine();
		}
		
		line = scanner.nextLine();
		while (!line.trim().equals(".")) {
			String[] exitInfo = line.split(" ", 3);
			int exitIndex;
			if (exitInfo[0].equals("N")) exitIndex = Room.NORTH;
			else if (exitInfo[0].equals("S")) exitIndex = SOUTH;
			else if (exitInfo[0].equals("E")) exitIndex = EAST;
			else exitIndex = WEST;
			
			exitDestinations[exitIndex] = Integer.parseInt(exitInfo[1]);
			exitDescriptions[exitIndex] = exitInfo[2];
			line = scanner.nextLine();
		}
	}



	public String getTitle() {
		return title;
	}

	public String getDescription() {
		return description;
	}

	public int getExitDestination(int direction) {
		return exitDestinations[direction];
	}
	
	public String getExitDescription(int direction) {
		return exitDescriptions[direction];
	}
}
