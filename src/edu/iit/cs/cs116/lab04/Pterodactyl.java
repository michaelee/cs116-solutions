package edu.iit.cs.cs116.lab04;

public class Pterodactyl extends Monster {
	public Pterodactyl() {
		hitPoints = 15;
		strength  = 50;
	}
	
	public String getName() {
		return "Pterodactyl";
	}

	public void processPlayerAttack(PlayerInfo player) {
		double playerDmg  = player.getStrength() * Math.random();
		
		if (Math.random() >= 0.5) {
			double monsterDmg = baseDamage();
			System.out.printf("The %s flies out of range and dives at you for %.2f damage!\n", 
					getName(), monsterDmg);
			player.setHitPoints(player.getHitPoints() - monsterDmg);
			recordDamageInflicted(monsterDmg);
		} else {
			hitPoints -= playerDmg;
			recordDamageTaken(playerDmg);
			System.out.printf("You hit the %s for %.2f damage!\n", getName(), playerDmg);			
		}		
	}	
}
