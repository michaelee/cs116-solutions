package edu.iit.cs.cs116.lab04;

public class Vampire extends Monster { // a Vampire "is-a" Monster
	// Vampire (the subclass) "inherits" its base behavior from Monster (the superclass)
	// Vampire has (at minimum) the same API as its superclass
	int lives;
	
	public Vampire() {
		super();
		hitPoints = 20.0;
		strength  = 20.0;
		lives = 2;
	}
	
	// override the original Monster's implementation of the following methods 
	
	public String getName() {
		return "Vampire";
	}
	
	public boolean isDead() {
		if (hitPoints > 0.0) {
			return false;
		} else if (hitPoints <= 0.0 && lives > 1) {
			System.out.println("The Vampire dies, but comes back to life!");
			hitPoints = 20.0;
			lives -= 1;
			return false;
		} else {
			return true;
		}
	}

}
