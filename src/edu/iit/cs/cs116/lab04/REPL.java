package edu.iit.cs.cs116.lab04;

import java.util.Scanner;
import java.util.Random;

public class REPL {
	static Scanner stdin;
	
	public static void doBattle(PlayerInfo player, Monster monster) {
		boolean battleOver = false;

		System.out.println("Battle commands: i=inspect, a=attack, d=defend, h=help:");
		
		while (!battleOver) {
			System.out.printf("[ %6.2f HP ] > ", player.getHitPoints());
			String cmd = stdin.nextLine();
			switch(cmd) {
			case "i":
				monster.processPlayerInspect(player);
				break;
			case "a":
				System.out.println("You attack!");
				monster.processPlayerAttack(player);
				break;
			case "d":
				System.out.println("You defend!");
				monster.processPlayerDefend(player);
				break;
			case "h":
				System.out.println("Battle commands: i=inspect, a=attack, d=defend, h=help:");
				break;
			default:
			}
			if (player.isDead()) {
				System.out.println("You died!");
				battleOver = true;
			} else if (monster.isDead()) {
				System.out.println("You vanquished the " + monster.getName() + "!");
				battleOver = true;				
			}
		}		
	}

	public static void main(String[] args) throws Exception {
		PlayerInfo player = new PlayerInfo(100, 10);
		Monster monster;
		Random rand = new Random();
		int kills = 0;
		Class[] monsterClasses = { Vampire.class, Pterodactyl.class };
		Monster[] vanquished = new Monster[100];
		
		stdin = new Scanner(System.in);
		
		while (!player.isDead()) {
			monster = (Monster)monsterClasses[rand.nextInt(monsterClasses.length)].newInstance();
			System.out.println("\nYou encounter a " + monster.getName());
			doBattle(player, monster);
			if (!player.isDead()) {
				vanquished[kills] = monster;
				kills += 1;
			}
		}
		
		System.out.printf("\nYou killed %d monsters before falling in battle.\n", kills);
		System.out.println("\nMonster roll call:");
		for (int i=0; i<kills; i++) {
			System.out.printf("%d. %s <Strength=%5.2f, Hits taken=%d, Damage taken/inflicted=%.2f/%.2f>\n", 
					i+1, vanquished[i].getName(), 
					vanquished[i].getStrength(), 
					vanquished[i].getHitsTaken(), 
					vanquished[i].getDamageTaken(),
					vanquished[i].getDamageInflicted());
		}
		
		stdin.close();
	}

}
