package edu.iit.cs.cs116.lab04;

public class PlayerInfo {
	double hitPoints;
	double strength;

	public PlayerInfo(int hitPoints, int strength) {
		this.hitPoints = hitPoints;
		this.strength = strength;
	}
	
	public double getHitPoints() {
		return hitPoints;
	}

	public void setHitPoints(double hitPoints) {
		this.hitPoints = hitPoints;
	}
	
	public double getStrength() {
		return strength;
	}

	public void setStrength(double strength) {
		this.strength = strength;
	}
	
	public boolean isDead() {
		return hitPoints <= 0.0;
	}
}
