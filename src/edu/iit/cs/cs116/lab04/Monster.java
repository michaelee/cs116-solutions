package edu.iit.cs.cs116.lab04;

public abstract class Monster {
	double hitPoints;
	double strength;
	int anger;
	int angerThreshold;
	int hitsTaken;
	double damageInflicted;
	double damageTaken;
	
	public Monster() {
		hitPoints      = 100;
		strength       = 35;
		anger          = 0;
		angerThreshold = 10;
		hitsTaken      = 0;
		damageInflicted = 0.0;
		damageTaken     = 0.0;
	}
	
	public abstract String getName();

	public boolean isDead() {
		return hitPoints <= 0.0;
	}
	
	protected double baseDamage() {
		return strength * Math.random();
	}
	
	protected void recordDamageInflicted(double dmg) {
		damageInflicted += dmg;
	}
	
	protected void recordDamageTaken(double dmg) {
		hitsTaken   += 1;
		damageTaken += dmg;
	}
	
	public void processPlayerAttack(PlayerInfo player) {
		double playerDmg  = player.getStrength() * Math.random();
		
		hitPoints -= playerDmg;
		recordDamageTaken(playerDmg);
		System.out.printf("You hit the %s for %.2f damage!\n", getName(), playerDmg);
		if (!isDead()) {
			anger += 3;
			if (anger >= angerThreshold) {
				double monsterDmg = baseDamage();
				System.out.printf("The %s retaliates for %.2f damage!\n", getName(), monsterDmg);
				player.setHitPoints(player.getHitPoints() - monsterDmg);
				recordDamageInflicted(monsterDmg);
				anger = 0;
			}
		}
	}
	
	public void processPlayerDefend(PlayerInfo player) {
		anger += 1;
		if (anger >= angerThreshold) {
			double monsterDmg = baseDamage() / 5.0; // dampen damage
			System.out.printf("The %s hits you for %.2f damage!\n", getName(), monsterDmg);
			player.setHitPoints(player.getHitPoints() - monsterDmg);
			recordDamageInflicted(monsterDmg);
			anger = 0;
		} else {
			System.out.printf("The %s hangs back.\n", getName());
		}
	}
	
	public void processPlayerInspect(PlayerInfo player) {
		System.out.printf("The %s has %.2f HP left.\n", getName(), hitPoints);
	}

	public int getHitsTaken() {
		return hitsTaken;
	}

	public double getDamageInflicted() {
		return damageInflicted;
	}
	
	public double getDamageTaken() {
		return damageTaken;
	}
	
	public double getStrength() {
		return strength;
	}
	
	
}
