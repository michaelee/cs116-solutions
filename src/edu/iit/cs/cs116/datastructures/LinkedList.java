package edu.iit.cs.cs116.datastructures;

public class LinkedList implements List {
	
	public class LinkedListNode {
		Object datum;
		LinkedListNode next;
		
		public LinkedListNode(Object o, LinkedListNode n) {
			datum = o;
			next  = n;
		}
	} // end of inner class declaration
	
	LinkedListNode head, tail;
	int count;
	
	public LinkedList() {
		head = null;
		tail = null;
		count = 0;
	}
	
	public void add(Object datum) {
		if (count == 0) {
			head = new LinkedListNode(datum, null);
			tail = head;
		} else {
			tail.next = new LinkedListNode(datum, null);
			tail = tail.next;
		}
		count += 1;
	}
	
	public int count() {
		return count;
	}
	
	public Object get(int idx) {
		LinkedListNode n = head;
		for (int i=0; i<idx; i++) {
			n = n.next;
		}
		return n.datum;
	}
	
	public void insert(Object datum, int idx) {
		if (idx == 0) {
			add(datum); // FIXME: this incorrectly inserts at the beginning
		} else {
			LinkedListNode n = head;
			// advance n to the position before the insertion spot
			for (int i=0; i<idx-1; i++) {
				n = n.next;
			}		
			LinkedListNode newnode = new LinkedListNode(datum, n.next);
			n.next = newnode;
			count += 1;
		}
		
		// FIXME: I don't work correctly if inserting at the tail!
	}
	
	public void remove(int idx) {
		if (idx == 0) {
			head = head.next;
		} else {
			LinkedListNode n = head;
			// advance n to the position before the node to remove
			for (int i=0; i<idx-1; i++) {
				n = n.next;
			}		
			n.next = n.next.next;
		}
		count -= 1;
		
		// FIXME: I don't work correctly if removing the tail!
	}

	public static void main(String[] args) {
		List l = new LinkedList();
		
		for (int i=0; i<10; i++) {
			l.add(String.valueOf(i));
		}
		
		l.insert("a", 1);
		l.insert("b", 5);
		l.insert("c", 12);
		
		for (int i=0; i<l.count(); i++) {
			System.out.println("Index " + i + " : " + l.get(i));
		}
	}
}
