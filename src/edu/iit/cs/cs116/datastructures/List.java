package edu.iit.cs.cs116.datastructures;

public interface List<T> {
	public void add(T datum);
	
	public void insert(T datum, int idx);
		
	public T get(int idx);
	
	public void remove(int idx);
	
	public int count();
	
	// TODO:
	
//	public void addAll(List from);
//	public void prepend(Object o);
//	
//	public void reverse();
//	
//	// the following two methods use the `equals` method to test for equality 
//	public boolean contains(Object key);
//	public void remove(Object key);
//	
//	// to implement the following, we will need to assume underlying data is `Comparable`
//	public void sort();
}
