package edu.iit.cs.cs116.datastructures;

public class ExpandingArray<T> implements List<T> {
	Object[] data; // backing storage for this data structure
	int cursor; // always going to contain the index of the "next" open slot

	public ExpandingArray() {
		data = new Object[10];
		cursor = 0;
	}
		
	// add the argument to the end of the data array
	public void add(T datum) {
		if (cursor >= data.length) {
			// expand the array if the cursor is past the end
			expandArray();
		}
		data[cursor] = datum;
		cursor += 1;
	}
	
	// insert the given datum at the provided index, sliding everything up by one slot
	public void insert(T datum, int idx) {
		if (idx > cursor) { 
			return;
		}
		if (cursor >= data.length) {
			// expand the array if the cursor is past the end
			expandArray();
		}
		for (int i=cursor; i>idx; i--) {
			data[i] = data[i-1];
		}
		data[idx] = datum;
		
		cursor += 1;
	}
	
	private void expandArray() {
		Object[] newdata = new Object[data.length * 2]; // double the size of the original
		for (int i=0; i<data.length; i++) {
			newdata[i] = data[i];
		}
		data = newdata;
	}
	
	@SuppressWarnings("unchecked")
	public T get(int idx) {
		return (T)data[idx];
	} 
	
	
	// remove the object at the given index; slide everything above it down by a slot
	public void remove(int idx) {
		if (idx >= cursor) { 
			return;
		}
		for (int i=idx; i<cursor-1; i++) {
			data[i] = data[i+1];
		}
		cursor -= 1;
	}
	
	public int count() {
		return cursor;
	}
	
	public static void main(String[] args) {
		List<String> l = new ExpandingArray<String>();

		for (int i=0; i<50; i++) {
			l.add(String.valueOf(i));
		}
				
		l.insert("goodbye", 3);
		l.remove(1);
			
		for (int i=0; i<l.count(); i++) {
			System.out.println(l.get(i));
		}
	}

}
