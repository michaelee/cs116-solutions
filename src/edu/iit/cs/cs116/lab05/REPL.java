package edu.iit.cs.cs116.lab05;

import edu.iit.cs.cs116.lab04.*;

import java.util.Scanner;
import java.io.*;

public class REPL {
	static Room currRoom;
	static Room[] rooms;

	static PlayerInfo player;
	static Monster[] vanquished;
	
	static int kills;
	
	static Scanner stdin;

	public static void advanceToRoom(Room room) {
		currRoom = room;
		System.out.println("You are now in the " + currRoom.getTitle());
	}
	
	public static void doBattle(PlayerInfo player, Monster monster) {
		boolean battleOver = false;

		System.out.println("Battle commands: i=inspect, a=attack, d=defend, h=help:");
		
		while (!battleOver) {
			System.out.printf("[ %6.2f HP ] > ", player.getHitPoints());
			String cmd = stdin.nextLine();
			switch(cmd) {
			case "i":
				monster.processPlayerInspect(player);
				break;
			case "a":
				System.out.println("You attack!");
				monster.processPlayerAttack(player);
				break;
			case "d":
				System.out.println("You defend!");
				monster.processPlayerDefend(player);
				break;
			case "h":
				System.out.println("Battle commands: i=inspect, a=attack, d=defend, h=help:");
				break;
			default:
			}
			if (player.isDead()) {
				System.out.println("You died!");
				battleOver = true;
			} else if (monster.isDead()) {
				System.out.println("You vanquished the " + monster.getName() + "!");
				battleOver = true;				
			}
		}		
	}
	
	public static boolean processCommand(String cmd) {
		switch(cmd) {
		case "l":
			System.out.println("\n" + currRoom.getTitle() + ":\n");
			System.out.println("You are in " 
					+ currRoom.getDescription());
			if (currRoom.getExitDestination(Room.NORTH) != -1)
				System.out.println("To the north, there is "
						+ currRoom.getExitDescription(Room.NORTH));
			if (currRoom.getExitDestination(Room.SOUTH) != -1)
				System.out.println("To the south, there is "
						+ currRoom.getExitDescription(Room.SOUTH));
			if (currRoom.getExitDestination(Room.EAST) != -1)
				System.out.println("To the east, there is "
						+ currRoom.getExitDescription(Room.EAST));
			if (currRoom.getExitDestination(Room.WEST) != -1)
				System.out.println("To the west, there is "
						+ currRoom.getExitDescription(Room.WEST));
			System.out.println();
			break;
		case "n":
			if (currRoom.getExitDestination(Room.NORTH) == -1)
				System.out.println("Can't move north from here");
			else 
				advanceToRoom(rooms[currRoom.getExitDestination(Room.NORTH)-1]);
			break;
		case "s":
			if (currRoom.getExitDestination(Room.SOUTH) == -1)
				System.out.println("Can't move south from here");
			else 
				advanceToRoom(rooms[currRoom.getExitDestination(Room.SOUTH)-1]);
			break;
		case "e":
			if (currRoom.getExitDestination(Room.EAST) == -1)
				System.out.println("Can't move east from here");
			else 
				advanceToRoom(rooms[currRoom.getExitDestination(Room.EAST)-1]);
			break;
		case "w":
			if (currRoom.getExitDestination(Room.WEST) == -1)
				System.out.println("Can't move west from here");
			else 
				advanceToRoom(rooms[currRoom.getExitDestination(Room.WEST)-1]);
			break;
		case "q":
			System.out.println("Quitting!");
			return true;
		}
		
		if (currRoom.getMonsterSpawnRate() > 0.0 
				&& Math.random() > currRoom.getMonsterSpawnRate()) {
			String[] monsterTypes = currRoom.getMonsterTypes();
			String monsterName = monsterTypes[(int)(Math.random() * monsterTypes.length)];
			System.out.println("You encounter a " + monsterName);	
			try {
				Monster monster = (Monster)Class.forName("edu.iit.cs.cs116.lab04." + monsterName).newInstance();
				doBattle(player, monster);
				if (player.isDead()) {
					printStats();
					return true;
				} else {
					vanquished[kills] = monster;
					kills += 1;
					return false;
				}
			} catch (InstantiationException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
		}
		return false;
	}
	
	public static void printStats() {
		System.out.printf("\nYou killed %d monsters before falling in battle.\n", kills);
		System.out.println("\nMonster roll call:");
		for (int i=0; i<kills; i++) {
			System.out.printf("%d. %s <Strength=%5.2f, Hits taken=%d, Damage taken/inflicted=%.2f/%.2f>\n", 
					i+1, vanquished[i].getName(), 
					vanquished[i].getStrength(), 
					vanquished[i].getHitsTaken(), 
					vanquished[i].getDamageTaken(),
					vanquished[i].getDamageInflicted());
		}
	}

	public static void main(String[] args) throws Exception {
		boolean gameOver = false;
		stdin = new Scanner(System.in);
		player = new PlayerInfo(100, 10);
		vanquished = new Monster[100];
		kills = 0;
		Scanner fileScanner = new Scanner(new File("datafiles/rooms-v3.dat"));
		
		rooms = new Room[fileScanner.nextInt()];
		fileScanner.nextLine(); // newline after int
		fileScanner.nextLine(); // throw away '.'
		
		for (int i=0; i<rooms.length; i++) {
			rooms[i] = new Room(fileScanner);
		}
		
		fileScanner.close();
		
		currRoom = rooms[0];
		
		while (!gameOver) { // REPL
			System.out.print("Enter a command: ");
			String cmd = stdin.nextLine().trim();
			gameOver = processCommand(cmd);
		}
		stdin.close();
	}
}
